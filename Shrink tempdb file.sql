USE radar_db;
GO
-- Truncate the log by changing the database recovery model to SIMPLE.
ALTER DATABASE radar_db
SET RECOVERY SIMPLE;
GO
-- Shrink the truncated log file to 1 MB.
DBCC SHRINKFILE (radar_db_log, 1);
GO
-- Reset the database recovery model.
ALTER DATABASE radar_db
SET RECOVERY FULL;
GO

-- ��� ���
USE tempdb 
GO 
DBCC SHRINKFILE (tempdev, 5) 


--select * from sys.sysobjects