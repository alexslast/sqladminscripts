USE [Database_health]
GO
/****** Object:  StoredProcedure [dbo].[spShrinkAllDatabaseLogs]    Script Date: 2/18/2019 6:19:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER procedure [dbo].[spShrinkAllDatabaseLogs]
as
begin


CREATE TABLE #CommandQueue
(
    ID INT IDENTITY ( 1, 1 )
    , SqlStatement VARCHAR(1000)
	, SqlLogStatement VARCHAR(1000)
)

INSERT INTO    #CommandQueue
(
    SqlStatement
)
SELECT distinct
		'USE ' + A.name + ';

		ALTER DATABASE ' + A.name + '
		SET RECOVERY SIMPLE;

		DBCC SHRINKFILE (' + A.name + '_Log, 1);

		ALTER DATABASE ' + A.name + '
		SET RECOVERY FULL;'
FROM
    sys.databases A
    INNER JOIN sys.master_files B
    ON A.database_id = B.database_id
WHERE
    A.name NOT IN ( 'master', 'model', 'msdb', 'tempdb' )
	and a.state=0
	
DECLARE @id INT

SELECT @id = MIN(ID)
FROM #CommandQueue

WHILE @id IS NOT NULL
BEGIN
    DECLARE @sqlStatement VARCHAR(1000)
    
    SELECT
        @sqlStatement = SqlStatement
    FROM
        #CommandQueue
    WHERE
        ID = @id

    PRINT 'Executing ''' + @sqlStatement + '''...'

    EXEC (@sqlStatement)

    DELETE FROM #CommandQueue
    WHERE ID = @id

    SELECT @id = MIN(ID)
    FROM #CommandQueue
END
end
