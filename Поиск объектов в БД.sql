SELECT OBJECT_SCHEMA_NAME(S.object_id) AS [Schema], OBJECT_NAME(S.object_id) AS [Object] 
FROM sys.sql_modules AS S WHERE S.definition LIKE '%tblAffiliations%'
ORDER BY 1, 2
