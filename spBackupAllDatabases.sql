USE [Database_health]
GO
/****** Object:  StoredProcedure [dbo].[spBackupAllDatabases]    Script Date: 2/18/2019 6:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[spBackupAllDatabases]	
AS
BEGIN

DECLARE @name NVARCHAR(50) -- database name  
DECLARE @path NVARCHAR(256) -- path for backup files  
DECLARE @dbPath NVARCHAR(256) -- path for backup files  
DECLARE @fileName NVARCHAR(256) -- filename for backup  
DECLARE @fileDate NVARCHAR(20) -- used for file name
 
-- specify database backup directory
SET @path = 'I:\MSSQL11.MSSQLSERVER\MSSQL\Backup'  
 
-- specify filename format
SELECT @fileDate = CONVERT(VARCHAR(20),GETDATE(),112) 

-- Get directory
DECLARE @DirTree TABLE (subdirectory nvarchar(255), depth INT)
INSERT INTO @DirTree(subdirectory, depth)
EXEC master.sys.xp_dirtree @path
 
DECLARE db_cursor CURSOR READ_ONLY FOR  
SELECT name 
FROM master.dbo.sysdatabases 
WHERE name NOT IN ('master','model','msdb','tempdb')  -- exclude these databases
and status=65536 -- work on onyl active databases
 
OPEN db_cursor   
FETCH NEXT FROM db_cursor INTO @name   
 
WHILE @@FETCH_STATUS = 0   
BEGIN   

IF NOT EXISTS (SELECT 1 FROM @DirTree WHERE subdirectory = @name)
begin
SET @dbPath = @path+'\'+@name
EXEC master.dbo.xp_create_subdir @dbPath
end
   SET @fileName = @dbPath +'\'+ @name + '_' + @fileDate + '.BAK'  
   BACKUP DATABASE @name TO DISK = @fileName   
   FETCH NEXT FROM db_cursor INTO @name   
END   

 
CLOSE db_cursor   
DEALLOCATE db_cursor
END

