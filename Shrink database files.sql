declare @DbName nvarchar(100)=radar_db

-- Shrink mdb file

USE @DbName;
GO
-- Truncate the log by changing the database recovery model to SIMPLE.
ALTER DATABASE @DbName
SET RECOVERY SIMPLE;
GO
-- Shrink the truncated log file to 1 MB.
DBCC SHRINKFILE (@DbName, 1);
GO
-- Reset the database recovery model.
ALTER DATABASE @DbName
SET RECOVERY FULL;
GO

-- Shrink log file
USE @DbName;
GO
-- Truncate the log by changing the database recovery model to SIMPLE.
ALTER DATABASE @DbName
SET RECOVERY SIMPLE;
GO
-- Shrink the truncated log file to 1 MB.
DBCC SHRINKFILE (@DbName+'_log', 1);
GO
-- Reset the database recovery model.
ALTER DATABASE @DbName
SET RECOVERY FULL;
GO