declare @dbex table (name nvarchar(255))

insert @dbex (name) 
--select 'Radar_Db' 
--union all 
--select 'Site_Surveyor_DB' 
--union all 
select 'HotlineSystem' 

if object_id('tempdb..#schmex') is not null drop table #schmex

create table #schmex (dbname nvarchar(255), name nvarchar(255))



if object_id('tempdb..#objex') is not null drop table #objex

create table #objex (dbname nvarchar(255), name nvarchar(255))

insert #objex (dbname, name)
SELECT'dv1966', 'dvsys_log_backup'
UNION ALL SELECT'dv1966', 'dvsys_dynamic_field_delete_old_sections'


--AF = Aggregate function (CLR)
--C = CHECK constraint
--D = Default or DEFAULT constraint
--F = FOREIGN KEY constraint
--L = Log
--FN = Scalar function
--FS = Assembly (CLR) scalar-function
--FT = Assembly (CLR) table-valued function
--IF = In-lined table-function
--IT = Internal table
--P = Stored procedure
--PC = Assembly (CLR) stored-procedure
--PK = PRIMARY KEY constraint (type is K)
--RF = Replication filter stored procedure
--S = System table
--SN = Synonym
--SQ = Service queue
--TA = Assembly (CLR) DML trigger
--TF = Table function
--TR = SQL DML Trigger
--TT = Table type
--U = User table
--UQ = UNIQUE constraint (type is K)
--V = View
--X = Extended stored procedure
if object_id('tempdb..#objtype') is not null drop table #objtype

create table #objtype (objtype nvarchar(255), objtypedef nvarchar(255), [statement] nvarchar(255))

insert #objtype
SELECT 'FN', 'Scalar function', 'FUNCTION'
UNION ALL SELECT 'IF', 'In-lined table-function', 'FUNCTION'
UNION ALL SELECT 'TF', 'Table function', 'FUNCITON'
UNION ALL SELECT 'V', 'View', 'VIEW'
UNION ALL SELECT 'U', 'User table', 'TABLE'
UNION ALL SELECT 'P', 'Stored procedure', 'PROCEDURE'

if object_id('tempdb..#suspicious') is not null drop table #suspicious

create table #suspicious ([server] nvarchar(255), [database] nvarchar(255), [schema] nvarchar(255), [name] nvarchar(255), [object_id] int, objtypedef nvarchar(255), [create_date] datetime, [drop statement] nvarchar(max),[ref_count] int)

if object_id('tempdb..#missing') is not null drop table #missing

create table #missing ([server] nvarchar(255), [database] nvarchar(255), [Avg_Estimated_Impact] bigint, [Last_User_Seek] datetime, [TableName] nvarchar(255), [create statement] nvarchar(max))

if object_id('tempdb..#unused') is not null drop table #unused

create table #unused ([server] nvarchar(255), [database] nvarchar(255), [ObjectName] nvarchar(255), [IndexName] nvarchar(255), [IndexID] int, [UserSeek] int, [UserScans] int, [UserLooksups] int, [UserUpdates] int, [TableRows] int, [drop statement] nvarchar(max))

DECLARE db_cursor CURSOR
READ_ONLY
FOR SELECT name from sys.sysdatabases where dbid > 4 
and name COLLATE Cyrillic_General_CI_AS in (select name COLLATE Cyrillic_General_CI_AS from @dbex) 
and (512 & status) != 512 
and HAS_PERMS_BY_NAME(name, 'DATABASE', 'ANY') != 0 
and HAS_PERMS_BY_NAME('master', 'SERVER', 'VIEW SERVER STATE') != 0

DECLARE @query nvarchar(max)
DECLARE @query2 nvarchar(max)
DECLARE @query3 nvarchar(max)
DECLARE @dbname nvarchar(255)

DECLARE @ref_count int

OPEN db_cursor

FETCH NEXT FROM db_cursor INTO @dbname
WHILE (@@fetch_status <> -1)
BEGIN
	IF (@@fetch_status <> -2)
	BEGIN
		-- lets determine any suspicious object names and schemas
		SELECT @query = 'insert #suspicious select ''' + @@servername + ''' server, ''' + @dbname + ''' [database], sh.name [schema], s.name, object_id, ot.objtypedef,s.create_date,' +		
		'''USE [' + @dbname + '];DROP '' + ot.[statement] COLLATE Cyrillic_General_CI_AS + '' ['' + sh.name COLLATE Cyrillic_General_CI_AS + ''].['' + s.name COLLATE Cyrillic_General_CI_AS + '']'' [drop statement], 0' + 
		' from ' + @dbname + '.sys.objects s inner join #objtype ot on s.type COLLATE Cyrillic_General_CI_AS = ot.objtype COLLATE Cyrillic_General_CI_AS' +		
		' inner join ' + @dbname + '.sys.schemas sh on s.schema_id = sh.schema_id' +
		' where s.name COLLATE Cyrillic_General_CI_AS not in (select objex.name COLLATE Cyrillic_General_CI_AS from #objex objex where dbname COLLATE Cyrillic_General_CI_AS = ''' + @dbname + ''' COLLATE Cyrillic_General_CI_AS)' +		
		--' and sh.name COLLATE Cyrillic_General_CI_AS not in (select schmex.name COLLATE Cyrillic_General_CI_AS from #schmex schmex where dbname COLLATE Cyrillic_General_CI_AS = ''' + @dbname + ''' COLLATE Cyrillic_General_CI_AS)' +		
		' and (' +
		' (s.name like ''%old%'' and s.name not like ''%folder%'') or' + 
		' s.name like ''%bak%'' or' + 
		' s.name like ''%backup%'' or' + 
		' s.name like ''%bck%'' or' + 
		--' s.name like ''%new%'' or' + 
		' s.name like ''%tmp%'' or' + 
		' (s.name like ''%temp%'' and s.name not like ''%template%'' and s.name not like ''%temployee%'') or' + 
		' s.name like ''%test%'' or' + 
		--' name like ''%archive%'' or' + 
		' s.name like ''%Sheet%'' or' + 
		' len(s.name) < 3 or' + 
		' s.name like ''%$%'' or' + 
		' (s.name like ''%[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]%'' and s.name not like ''%-%-%-%-%'') or' + 
		' (s.name like ''%[0-9]'' and s.name not like ''imp_s_%'' and s.name not like ''exp_%'' and s.name not like ''tme2_%'' and s.name not like ''dt_verstamp%'' ) or' +
		' sh.name != ''dbo''' +
		' )'

		EXEC (@query)


		DECLARE text_cursor CURSOR
		READ_ONLY
		FOR SELECT [object_id], name from #suspicious where [database] = @dbname

		DECLARE @oid int
		DECLARE @oname varchar(255)
		OPEN text_cursor

		FETCH NEXT FROM text_cursor INTO @oid, @oname
		WHILE (@@fetch_status <> -1)
		BEGIN
			IF (@@fetch_status <> -2)
			BEGIN
				SELECT @query3 = 'UPDATE #suspicious set ref_count = (SELECT COUNT(DISTINCT id) ooid FROM [' + @dbname + '].[sys].[syscomments] where text like ''%' + @oname + '%'') where [database] = ''' + @dbname + ''' and object_id = ' + convert(nvarchar(max), @oid)

				exec (@query3)
			END
			FETCH NEXT FROM text_cursor INTO @oid, @oname
		END

		CLOSE text_cursor
		DEALLOCATE text_cursor

		-- lets determine missing indexes
		INSERT #missing
		SELECT 
		 @@servername [server],
		 @dbname [database],
		convert(bigint, dm_migs.avg_user_impact*(dm_migs.user_seeks+dm_migs.user_scans)) Avg_Estimated_Impact,
		dm_migs.last_user_seek AS Last_User_Seek,
		OBJECT_NAME(dm_mid.OBJECT_ID,dm_mid.database_id) AS [TableName],
		'USE [' + @dbname + '];CREATE INDEX [IX_' + OBJECT_NAME(dm_mid.OBJECT_ID,dm_mid.database_id) + '_'
		+ REPLACE(REPLACE(REPLACE(ISNULL(dm_mid.equality_columns,''),', ','_'),'[',''),']','') +
		CASE
		WHEN dm_mid.equality_columns IS NOT NULL AND dm_mid.inequality_columns IS NOT NULL THEN '_'
		ELSE ''
		END
		+ REPLACE(REPLACE(REPLACE(ISNULL(dm_mid.inequality_columns,''),', ','_'),'[',''),']','')
		 + ']'
		+ ' ON ' + dm_mid.statement
		+ ' (' + ISNULL (dm_mid.equality_columns,'')
		 + CASE WHEN dm_mid.equality_columns IS NOT NULL AND dm_mid.inequality_columns IS NOT NULL THEN ',' ELSE
		'' END
		+ ISNULL (dm_mid.inequality_columns, '')
		 + ')'
		+ ISNULL (' INCLUDE (' + dm_mid.included_columns + ')', '') AS Create_Statement
		FROM sys.dm_db_missing_index_groups dm_mig
		INNER JOIN sys.dm_db_missing_index_group_stats dm_migs
		ON dm_migs.group_handle = dm_mig.index_group_handle
		INNER JOIN sys.dm_db_missing_index_details dm_mid
		ON dm_mig.index_handle = dm_mid.index_handle
		INNER JOIN sys.sysdatabases DB
		ON dm_mid.database_id = DB.dbid
		WHERE DB.name = @dbname
		ORDER BY DB.name, Avg_Estimated_Impact DESC

		-- lets determine unused indexes
		select @query2 = 'USE ' + @dbname + ';' +
		'INSERT #unused SELECT ' +
		'@@servername [server],' +
		 '''' + @dbname + '''[database],' +		 
		 'o.name AS ObjectName' +
		', i.name AS IndexName' +
		', i.index_id AS IndexID' +
		', dm_ius.user_seeks AS UserSeek' +
		', dm_ius.user_scans AS UserScans' +
		', dm_ius.user_lookups AS UserLookups' +
		', dm_ius.user_updates AS UserUpdates' +
		', p.TableRows' +
		', ''USE [' + @dbname+ '];DROP INDEX '' + QUOTENAME(i.name)' +
		 '+ '' ON '' + QUOTENAME(s.name) + ''.'' + QUOTENAME(OBJECT_NAME(dm_ius.OBJECT_ID)) AS ''drop statement''' +
		'FROM ' + @dbname + '.sys.dm_db_index_usage_stats dm_ius' +
		' INNER JOIN ' + @dbname + '.sys.indexes i ON i.index_id = dm_ius.index_id AND dm_ius.OBJECT_ID = i.OBJECT_ID' +
		' INNER JOIN ' + @dbname + '.sys.objects o ON dm_ius.OBJECT_ID = o.OBJECT_ID' +
		' INNER JOIN ' + @dbname + '.sys.schemas s ON o.schema_id = s.schema_id' +
		' INNER JOIN ' + @dbname + '.sys.sysdatabases DB' +
		' ON dm_ius.database_id = DB.dbid' +
		' INNER JOIN (SELECT SUM(p.rows) TableRows, p.index_id, p.OBJECT_ID' +
		' FROM ' + @dbname + '.sys.partitions p GROUP BY p.index_id, p.OBJECT_ID) p' +
		' ON p.index_id = dm_ius.index_id AND dm_ius.OBJECT_ID = p.OBJECT_ID' +
		' WHERE OBJECTPROPERTY(dm_ius.OBJECT_ID,''IsUserTable'') = 1' +
		 ' AND i.type_desc = ''nonclustered''' +
		' AND i.is_primary_key = 0' +
		' AND i.is_unique_constraint = 0' +
		' AND DB.name = ''' + @dbname + '''' +
		' AND dm_ius.user_seeks + dm_ius.user_scans + dm_ius.user_lookups = 0 and dm_ius.user_updates > 0' +
		' ORDER BY (dm_ius.user_seeks + dm_ius.user_scans + dm_ius.user_lookups) ASC'

		EXEC (@query2)

	END
	FETCH NEXT FROM db_cursor INTO @dbname
END

CLOSE db_cursor
DEALLOCATE db_cursor
GO



SELECT * FROM #suspicious ORDER BY [database] ASC
SELECT * FROM #missing ORDER BY [database] ASC, Avg_Estimated_Impact DESC
SELECT * FROM #unused ORDER BY [database] ASC, [UserUpdates] Desc
